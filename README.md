# Express + MySQL boilerplate

## Run the app locally (in development):
```
yarn dev
```
note: there's a `yarn start` but that is for **production**

## Add your MySQL credentials
Create a file in the root of the project called `.env` with the following:

```
MYSQL_HOST=your host
MYSQL_USERNAME=your username
MYSQL_PASSWORD=your password
MYSQL_DB=your db name
```